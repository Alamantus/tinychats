<?php
require_once __DIR__ . '/vendor/autoload.php';

use Alamantus\TinyChats\TinyChats;

try {
  (new TinyChats)->handleRequest();
} catch (Exception $ex) {
  header('HTTP/1.1 ', $ex->getMessage());
  exit;
}
