<?php
namespace Alamantus\TinyChats;

require_once(__DIR__ . '/config.php');

use \Exception;
use \PDO;

class Db {
  private $dsn;
  private $pdo;

  const table = 'comments';
  const columns = [
    'domain' => [
      'type' => ['TEXT', 'NOT NULL'],
    ],
    'path' => [
      'type' => ['TEXT'],
    ],
    'parent' => [
      'type' => ['INTEGER'],
    ],
    'name' => [
      'type' => ['TEXT'],
    ],
    'email' => [
      'type' => ['TEXT'],
    ],
    'comment' => [
      'type' => ['TEXT'],
    ],
    'ip_hash' => [
      'type' => ['TEXT'],
    ],
    'session' => [
      'type' => ['TEXT'],
    ],
    'moment' => [
      'type' => ['REAL'],
    ],
  ];

  public function __construct() {
    $this->dsn = $this->buildPdoConnectionString();
    $this->pdo = new PDO($this->dsn);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    try {
      $this->pdo->query('SELECT 1 FROM ' . static::table . ' LIMIT 1');
    } catch (Exception $e) {
      throw new Exception('Database is not set up');
    }
  }

  private function buildPdoConnectionString() {
    $dsn = DB_TYPE . ':';
    if (DB_TYPE === 'sqlite') {
      return $dsn . DB_DATABASE;
    }
    $dsn .= 'host=' . DB_HOST;
    if (DB_PORT) $dsn .= ';port=' . DB_PORT;
    $dsn .= ';dbname=' . DB_DATABASE;
    if (DB_TYPE === 'pgsql') {
      $dsn .= "options='--client_encoding=UTF8'";
    } else if (DB_TYPE === 'mysql') {
      $dsn .= ';charset=UTF8';
    }
    return $dsn;
  }

  public static function setupDatabase() {
    $columns = array_map(function($column, $details) {
      return "`{$column}` " . implode(' ', $details['type']);
    }, array_keys(static::columns), array_values(static::columns));
    $rawColumns = (implode(', ', $columns));
    $db = static::getSingleton();
    $table = static::table;
    $db->prepare("CREATE TABLE {$table} ({$rawColumns});")->execute();
    $db->prepare("CREATE INDEX idx_{$table}_ip_hash ON {$table} (`ip_hash`);")->execute();
    $db->prepare("CREATE INDEX idx_{$table}_session ON {$table} (`ip_hash`, `session`);")->execute();
    $db->prepare("CREATE INDEX idx_{$table}_domain ON {$table} (`domain`);")->execute();
    $db->prepare("CREATE INDEX idx_{$table}_path ON {$table} (`domain`, `path`);")->execute();
  }

  public function prepare($sql) {
    return $this->pdo->prepare($sql);
  }

  public function canInsert($ipHash = null) {
    $stmt = $this->prepare('SELECT moment FROM ' . static::table . ' ' . (isset($ipHash) ? 'WHERE ip_hash=?' : '') . ' ORDER BY moment DESC LIMIT 1;');
    $stmt->execute(isset($ipHash) ? [$ipHash] : []);
    $val = $stmt->fetch();
    // Don't log requests within 1/10th of a moment
    return $val && (microtime(true) - doubleval($val['moment']) > 0.1);
  }

  // public function backup() {
  //   if (file_exists($dbFilename)) {
  //     // back up existing db
  //     $backupName = $dbFilename . '.backup';
  //     $maxlength = 80;
  //     while (file_exists($backupName) && strlen($backupName) < $maxlength) {
  //         // move existing backups to have more and more '.backups' on them
  //         $backupBackupName = $backupName . '.backup';
  //         rename($backupName, $backupBackupName);
  //         $backupName = $backupBackupName;
  //     }
  //     if (strlen($backupName) > $maxlength) {
  //         header('HTTP/1.1 409 Conflict');
  //         echo 'Too many backup files. Delete some old ones.';
  //         exit;
  //     }
  //     rename($dbFilename, $dbFilename . '.backup');
  //   }
  // }

  public static function getSingleton() {
    $instance = 'TinyChatsDbInstance';
    if (!isset($_GLOBALS[$instance]) || !($_GLOBALS[$instance] instanceof static)) {
      $_GLOBALS[$instance] = new static();
    }
    return $_GLOBALS[$instance];
  }

  public static function insert($values) {
    $db = static::getSingleton();
    $ip = $_SERVER['REMOTE_ADDR'];
    $ipHash = isset($ip) ? md5($ip) : null;
    if (!$db->canInsert($ipHash)) {
      throw new \Exception('429 Too Many Requests');
    }
    $table = static::table;
    $columns = array_map(fn ($column) => "`{$column}`", array_keys(static::columns));
    $rawColumns = implode(',', $columns);
    $values = array_map(fn ($columns) => '?', $columns);
    $rawValues = implode(',', $values);
    $stmt = $db->prepare(<<<SQL
      INSERT INTO {$table}({$rawColumns})
      VALUES({$rawValues})
    SQL);
    $moment = microtime(true);
    // Query values take the first letter of each column
    $session = isset($values['s']) && !empty(trim($values['s'])) ? trim($values['s']) : md5($moment);
    $stmt->execute([
      $values['d'],
      isset($values['p']) && !empty(trim($values['p'])) ? trim($values['p']) : '/',
      isset($values['r']) && !empty($values['r']) ? intval($values['r']) : null,
      isset($values['n']) && !empty(trim($values['n'])) ? trim($values['n']) : null,
      isset($values['e']) && !empty(trim($values['e'])) ? trim($values['e']) : null,
      isset($values['c']) && !empty(trim($values['c'])) ? trim($values['c']) : null,
      $ipHash,
      $session,
      $moment,
    ]);
    return $session;
  }

  public static function update($match, $values) {
    $db = static::getSingleton();
    $ip = $_SERVER['REMOTE_ADDR'];
    $ipHash = isset($ip) ? md5($ip) : null;
    if (!$db->canInsert($ipHash)) {
      throw new \Exception('429 Too Many Requests');
    }
    $bindings = [];
    $set = [];
    foreach($values as $column => $value) {
      $set[] = $column . '=?';
      $bindings[] = $value;
    }
    $rawSet = implode(', ', $set);
    $where = [];
    foreach($match as $column => $value) {
      $where[] = $column . '=?';
      $bindings[] = $value;
    }
    $rawWhere = empty($where) ? '' : 'WHERE ' . implode(' AND ', $where);
    $table = static::table;
    $stmt = $db->prepare(<<<SQL
      UPDATE {$table}
      SET {$rawSet}
      {$rawWhere}
    SQL);
    $stmt->execute($bindings);
    return $session;
  }

  public static function query($sql, $bindings = []) {
    $db = static::getSingleton();
    $stmt = $db->prepare($sql);
    $stmt->execute($bindings);
    $results = $stmt->fetchAll();
    return empty($results) ? $results : array_map(function ($result) {
      $values = [];
      foreach($result as $key => $value) {
        if (in_array($key, array_keys(static::columns))) {
          $values[$key] = $value;
        }
      }
      return $values;
    }, $results);
  }

  public static function comments($where = [], $bindings = []) {
    $table = static::table;
    $whereRaw = empty($where) ? '' : 'WHERE ' . join(' AND ', $where);
    return static::query(<<<SQL
      SELECT * FROM {$table}
      {$whereRaw}
      ORDER BY moment DESC
    SQL, $bindings);
  }

  public static function formatLog($log) {
    $log['moment'] = Db::formatMoment($log['moment']);
    if (isset($log['extra']['pageTime'])) {
      $log['pageTime'] = Db::formatPageTime($log['extra']['pageTime']);
      unset($log['extra']['pageTime']);
    }
    return $log;
  }

  public static function formatMoment($moment) {
    $time = date_create_from_format('U.u', strval($moment));
    return $time ? $time->format('Y-m-d H:i:s.u') : $moment;
  }

  public static function formatPageTime($pageTime) {
    $hours = floor($pageTime / 3600);
    $minutes = floor(($pageTime / 60) % 60);
    $seconds = $pageTime % 60;
    return ($hours > 0 ? "$hours hours, " : '') . ($minutes > 0 ? "$minutes minutes, " : '') . "$seconds seconds";
  }
}
