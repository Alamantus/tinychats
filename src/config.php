<?php
define('DB_TYPE', 'sqlite'); // sqlite, mysql, pgsql
define('DB_HOST', 'localhost');
define('DB_PORT', null);
define('DB_DATABASE', 'tinychats.sqlite3');
define('DB_USERNAME', 'username');
define('DB_PASSWORD', 'password');
define('ADMIN_PASS', '');
define('ALLOWED_DOMAINS', []);
define('JS_CACHE_MAX_AGE', 86400); // time in seconds
