<?php
namespace Alamantus\TinyChats;

require_once(__DIR__ . '/config.php');

use Alamantus\TinyChats\Admin;
use Alamantus\TinyChats\Db;
use \Exception;

class TinyChats {
  public function handleRequest() {
    $this->cors();

    if (isset($_GET['admin'])) {
      (new Admin)->handleRequest();
      exit;
    }

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
      $this->serveJavaScript();
      exit;
    }

    try {
      $this->getComments();
    } catch (\Exception $ex) {
      throw $ex;
    }
  }

  private function cors() {
    // Handle CORS for allowed domains
    if (isset($_SERVER['HTTP_ORIGIN'])) {
      $originUrl = parse_url($_SERVER['HTTP_ORIGIN']);
      if (in_array($originUrl['host'], ALLOWED_DOMAINS)) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
      }
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
          header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
      }
      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
          header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
      }
      exit;
    }
  }

  public function serveJavaScript() {
    $host = $_SERVER['HTTP_HOST'];
    $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
    $commentUrl = "{$protocol}://{$host}{$_SERVER['REQUEST_URI']}";
    $js = file_get_contents(__DIR__ . '/index.js');
    $js = str_replace('{$commentUrl}', $commentUrl, $js);
    header('Access-Control-Max-Age: ' . JS_CACHE_MAX_AGE);
    header('Content-Type: text/javascript; charset=UTF-8');
    echo $js;
    exit;
  }

  public function getComments() {
    $json = file_get_contents('php://input');
    $request = json_decode($json, true);

    // Check if it can comment
    if (!isset($request['d']) || empty($request['d'])) {
      throw new Exception('400 Bad Request');
    }

    $domain = trim($request['d']);
    if (!in_array($domain, ALLOWED_DOMAINS)) {
      throw new Exception('401 Unauthorized');
    }

    if (isset($_GET['t'])) {
      $this->updateTime($request);
      exit;
    }

    try {
      $session = Db::insert($request);
      header('Content-Type: application/json; charset=UTF-8');
      echo json_encode($session);
      exit;
    } catch (\Exception $ex) {
      throw $ex;
    }
  }

  public function updateTime($request) {
    // Check if it can comment
    if (!isset($request['s']) || empty($request['s'])) {
      throw new Exception('400 Bad Request');
    }

    try {
      $where = [
        'domain = ?',
        'path = ?',
        'session = ?',
      ];
      $binding = [
        $request['d'],
        $request['p'],
        $request['s'],
      ];
      if (isset($request['q'])) {
        $where[] = 'query = ?';
        $binding[] = $request['q'];
      }
      $comments = Db::comments($where, $binding);
      if (!empty($comments)) {
        $comment = $comments[0];
        $match = [
          'domain' => $comment['domain'],
          'path' => $comment['path'],
          'session' => $comment['session'],
        ];
        if (isset($comment['query'])) {
          $match['query'] = $comment['query'];
        }
        $extra = $comment['extra'] ?? [];
        $extra['pageTime'] = microtime(true) - $comment['moment'];
        Db::update($match, [
          'extra' => json_encode($extra),
        ]);
        echo $extra['pageTime'];
      }
      exit;
    } catch (\Exception $ex) {
      throw $ex;
    }
  }
}
