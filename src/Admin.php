<?php
namespace Alamantus\TinyChats;

require_once(__DIR__ . '/config.php');

use Alamantus\TinyChats\Db;
use \Exception;

class Admin {
  private $loggedIn;
  private $baseUrl;

  public function __construct() {
    session_start();
    $this->loggedIn = !empty(ADMIN_PASS) && $_SESSION['loggedIn'];
    // Get the admin URL from the request URI
    $parsed_url = parse_url($_SERVER['REQUEST_URI']);
    $scheme = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass = ($user || $pass) ? "$pass@" : '';
    $path = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $this->baseUrl = "$scheme$user$pass$host$port$path?admin";
  }

  public function handleRequest() {
    switch ($_SERVER['REQUEST_METHOD']) {
      case 'POST': {
        $this->handlePost();
        break;
      }
      default: {
        if (empty(ADMIN_PASS)) {
          $this->showView('setup', [
            'requestUri' => $this->baseUrl . '=setup',
          ]);
        } else {
          if ($this->loggedIn) {
            $this->routePage();
          } else {
            $this->showView('login', [
              'requestUri' => $this->baseUrl . '=login',
            ]);
          }
        }
        break;
      }
    }
    exit;
  }

  private function handlePost() {
    switch ($_GET['admin']) {
      case 'login': {
        if (!empty($_POST['username'])) {
          header('HTTP/1.1 401 Unauthorized');
          echo 'Access Denied!';
          exit;
        }
        $_SESSION['loggedIn'] = password_verify($_POST['password'], ADMIN_PASS);
        header('Location: ' . $this->baseUrl, true, 303);
        break;
      }
      case 'logout': {
        session_destroy();
        header('Location: ' . $this->baseUrl, true, 303);
        break;
      }
      case 'setup': {
        $this->processSetup();
        break;
      }
      case 'settings': {
        $this->processSettings();
        break;
      }
    }
  }

  public function showView($viewName, $values = []) {
    $values['logoutUri'] = $this->baseUrl . '=logout';
    $html = file_get_contents(__DIR__ . '/views/' . $viewName . '.html');
    preg_match_all('/{{([^}]+)}}/', $html, $includeMatches);
    foreach($includeMatches[0] as $index => $includeMatch) {
      // var_dump($includeMatch);
      $includeFilename = $includeMatches[1][$index];
      if (file_exists(__DIR__ . '/views/includes/' . $includeFilename)) {
        $include = file_get_contents(__DIR__ . '/views/includes/' . $includeFilename);
        $html = str_replace($includeMatch, $include, $html);
      }
    }
    foreach($values as $variable => $value) {
      $html = str_replace('{$' . $variable . '}', $value, $html);
    }
    header('Content-Type: text/html; charset=UTF-8');
    echo $html;
    exit;
  }

  private function routePage() {
    $table = Db::table;
    $page = $_GET['page'] ?? 'admin';
    $requestDomain = $_GET['domain'] ?? null;
    $requestStart = !empty($_GET['start']) ? $_GET['start'] : null;
    $requestEnd = !empty($_GET['end']) ? $_GET['end'] : null;
    $pageData = [
      'page' => $page,
      'start' => $requestStart ?? '',
      'end' => $requestEnd ?? '',
    ];
    $filterUrlAppend = '';
    if ($requestStart) $filterUrlAppend .= '&start=' . urlencode($requestStart);
    if ($requestEnd) $filterUrlAppend .= '&end=' . urlencode($requestEnd);

    $where = [];
    $bindings = [];
    if (isset($requestDomain)) {
      $where[] = 'domain=?';
      $bindings[] = $requestDomain;
    }
    if (isset($requestStart)) {
      $where[] = 'moment>=?';
      $dateTime = date_create_from_format('Y-m-d H:i', str_replace('T', ' ', $requestStart));
      $bindings[] = $dateTime ? $dateTime->format('U.u') : 0;
    }
    if (isset($requestEnd)) {
      $where[] = 'moment<=?';
      $dateTime = date_create_from_format('Y-m-d H:i', str_replace('T', ' ', $requestEnd));
      $bindings[] = $dateTime ? $dateTime->format('U.u') : 0;
    }
    
    switch ($page) {
      default: {
        try {
          $whereRaw = empty($where) ? '' : 'WHERE ' . implode(' AND ', $where);
          $totalStmt = Db::getSingleton()->prepare("SELECT domain, COUNT(moment) AS total FROM {$table} {$whereRaw} GROUP BY domain");
          $totalStmt->execute($bindings);
          $totals = $totalStmt->fetchAll();
          $comments = Db::comments($where, $bindings);
        } catch (\Exception $ex) {
          $totals = [];
          $comments = [];
          var_dump($ex);
        }
        $totalsHtml = '<ul style="margin:10px 0;padding:0;list-style:none;display:flex;flex-direction:row;align-items:center;gap:10px;"><li>';
        if (is_null($requestDomain)) {
          $totalsHtml .= '<strong>Show All</strong>';
        } else {
          $totalsHtml .= '<a href="?admin' . $filterUrlAppend . '">Show All</a>';
        }
        foreach($totals as $row) {
          $totalsHtml .= '<li>';
          if ($row['domain'] != $requestDomain) {
            $totalsHtml .= '<a href="?admin&domain=' . urlencode($row['domain']) . $filterUrlAppend . '">' . $row['domain'] . '</a>';
          } else {
            $totalsHtml .= '<strong>' . $row['domain'] . '</strong>';
          }
          $totalsHtml .= ' (' . number_format($row['total']) . ' total)</li>';
        }
        $totalsHtml .= '</ul>';
        $breakdown = [];
        foreach($comments as $comment) {
          if (!isset($breakdown[$comment['domain']])) {
            $breakdown[$comment['domain']] = [];
          }
          if (!isset($breakdown[$comment['domain']][$comment['path']])) {
            $breakdown[$comment['domain']][$comment['path']] = [
              'totals' => ['visits' => 0, 'time' => 0],
              'comments' => [],
            ];
          }
          $breakdown[$comment['domain']][$comment['path']]['totals']['visits']++;
          $breakdown[$comment['domain']][$comment['path']]['totals']['time'] += isset($comment['extra']['pageTime']) ? $comment['extra']['pageTime'] : 0;
          $breakdown[$comment['domain']][$comment['path']]['comments'][] = $comment;
        }
        $breakdownHtml = '';
        foreach($breakdown as $domain => $pathData) {
          $breakdownHtml .= "<hr /><section style=\"max-width:800px;\"><h2>{$domain}</h2>
          <table><thead><tr><th>Path</th><th>Total Visits</th><th style=\"text-align:center;padding:0 5px;\">Total Time</th><th></th></tr></thead><tbody>";
          foreach($pathData as $path => $data) {
            $breakdownHtml .= "<tr><td><a href=\"//{$domain}{$path}\" target=\"_blank\" title=\"Visit Page\">{$path}</a></td><td style=\"text-align:center;padding:0 5px;\">{$data['totals']['visits']}</td><td>" . Db::formatPageTime($data['totals']['time']) . '</td><td><a href="?admin&page=rawlogs&domain=' . urlencode($domain) . '&path=' . urlencode($path) . $filterUrlAppend . '">Raw Path Logs</a></td></tr>';
          }
          $breakdownHtml .= '</tbody></table><p style="text-align:right;"><a href="?admin&page=rawlogs&domain=' . urlencode($domain) . $filterUrlAppend . '">See Raw Logs for Domain</a></p></section>';
        }
        $pageData['totals'] = $totalsHtml;
        $pageData['breakdown'] = empty($breakdown) ? '<hr /><p>No Results</p>' : $breakdownHtml;
        break;
      }
      case 'settings': {
        $pageData = [
          'settingsUri' => $this->baseUrl . '=settings',
          'allowedDomains' => implode(PHP_EOL, ALLOWED_DOMAINS),
        ];
        break;
      }
      case 'rawlogs': {
        $requestPath = $_GET['path'] ?? null;
        try {
          if (isset($requestPath)) {
            $where[] = 'path=?';
            $bindings[] = $requestPath;
          }
          $comments = Db::comments($where, $bindings);
          $comments = array_map(fn ($comment) => Db::formatLog($comment), $comments);
        } catch (\Exception $ex) {
          $comments = [];
          var_dump($ex);
        }
        $pageData['forDomain'] = isset($requestDomain)
            ? ' for ' . $requestDomain . ($requestPath ?? '')
            : '';
        $pageData['comments'] = empty($comments) ? 'No Results' : var_export($comments, true);
        break;
      }
    }
    $this->showView($page, $pageData);
    exit;
  }

  private function updateConfig($values = []) {
    $configPath = __DIR__ . '/config.php';
    $file = file_get_contents($configPath);

    foreach ($values as $constName => $value) {
      if (is_array($value)) $value =json_encode($value);
      else $value = "'{$value}'";
      $file = preg_replace(
        "/define\('" . $constName . "',.*/",
        "define('" . $constName . "', " . str_replace('$', '\\$', $value) . ');',
        $file
      );
    }
    file_put_contents($configPath, $file);
  }

  private function processSetup() {
    if (empty($adminPass)) {
      if (isset($_POST['password']) && !empty($_POST['password'])) {
        $domains = array_map(function ($domain) {
          return preg_replace('/[^a-z0-9\.\-]/', '', strtolower($domain)); // No international or special characters
        }, explode(PHP_EOL, ($_POST['domains'] ?? '')));
        $values = [
          'ADMIN_PASS' => password_hash($_POST['password'], PASSWORD_DEFAULT),
          'DB_DATABASE' => preg_replace('/[^\w\.\-\_]/', '', ($_POST['dbName'] ?? 'tinystats.sqlite3')), // Remove anything from dbName that's not alphanumeric or . - _
          'ALLOWED_DOMAINS' => $domains,
        ];
        $this->updateConfig($values);
    
        try {
          Db::getSingleton();
        } catch (\Exception $ex) {
          Db::setupDatabase();
        }
      }
      header('Location: ' . $this->baseUrl, true, 303);
      exit;
    }
  }

  private function processSettings() {
    if (isset($_POST['password']) && !empty($_POST['password']) && password_verify($_POST['password'], ADMIN_PASS)) {
      $domains = array_map(function ($domain) {
        return preg_replace('/[^a-z0-9\.\-]/', '', strtolower($domain)); // No international or special characters
      }, explode(PHP_EOL, ($_POST['domains'] ?? '')));
      $values = [
        // 'DB_DATABASE' => preg_replace('/[^\w\.\-\_]/', '', ($_POST['dbName'] ?? 'tinystats.sqlite3')), // Remove anything from dbName that's not alphanumeric or . - _
        'ALLOWED_DOMAINS' => $domains,
      ];
      if (isset($_POST['new_password']) && !empty($_POST['new_password'])) {
        $values['ADMIN_PASS'] = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
      }
      $this->updateConfig($values);

      header('Location: ' . $this->baseUrl . '&page=settings', true, 303);
      exit;
    }

    header('HTTP/1.1 405 Method Not Allowed');
    echo 'Password incorrect';
  }
}
