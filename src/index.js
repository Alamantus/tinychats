function TinyChats(tinyChatsElementId) {
  this.tinyChatsElementId = tinyChatsElementId ?? 'tinychats';
  this.el = document.getElementById(this.tinyChatsElementId);
  // If the TinyChats element is not on the page, cancel loading.
  if (!this.el) return;

  const self = this;
  this.session = null;
  this.allComments = [];
  let savedName = localStorage.getItem('name');

  this.initialize = function() {
    this.el.appendChild(this.getCommentForm());
    const comments = document.createElement('section');
    comments.className = 'replies';
    this.el.appendChild(comments);
    this.loadComments();
  }

  this.postComment = function (event) {
    event.preventDefault();
    const form = event.target;
    const name = form.name?.value?.trim() ?? savedName;
    const parent = form.parentId.value;
    const comment = form.comment.value.trim();
    const post = self.validatePost({ parent, name, comment });
    if (typeof post === 'string') {
      return;
    }

    if (!savedName) {
      if (!confirm(`The name "${name}" will be saved to use for future posts. Is this correct?`)) {
        return false;
      }
      localStorage.setItem('name', name);
      savedName = name;
      const mainFormNameFields = self.el.querySelectorAll('form input[name="name"]');
      Array.from(mainFormNameFields).forEach(function (fieldEl) {
        const nameEl = document.createElement('span');
        nameEl.innerHTML = `Posting as: ${savedName}`;
        const labelEl = fieldEl.parentNode;
        labelEl.parentNode.replaceChild(nameEl, labelEl);
      });
    }

    // Send post to store it and get its id
    // this.comment(form);
    const repliesEl = (parent ? document.getElementById('post' + parent) : self.el).querySelector('.replies');
    repliesEl.prepend(self.generatePost(post));

    if (parent) {
      // If posting a reply, re-display the reply button & remove the form
      form.parentNode.querySelector('button').style.display = '';
      form.parentNode.removeChild(form);
    } else {
      form.comment.value = '';
    }
  };

  this.validatePost = function(postData) {
    if (postData.name.length < 1) {
      return 'Name must have at least 1 letter.';
    }
    postData.comment = postData.comment.replace(/\s*\n(\s*\n+)/gs, '');
    if (postData.comment.length < 5) {
      return 'Please include more than 5 letters in your comment';
    }
    postData.moment = Date.now() * 1000;
    return postData;
  }

  this.getCommentForm = function(parentId = '') {
    const form = document.createElement('form');
    form.id = 'replyTo' + (parentId ? parentId : 'Post');
    form.innerHTML = `<input aria-hidden="true" style="position:absolute;left:99999px;max-width:0px;max-height0px;" name="password" />
      <input type="hidden" name="parentId" value="${parentId}" />
      ${!savedName
        ? `<label>
          Display Name
          <input type="text" name="name" minlength="1" required />
        </label>`
        : `<span>Posting as: ${savedName}</span>`
      }
      <label>
        Comment
        <textarea name="comment" minlength="5"></textarea>
      </label>
      <button type="submit">Send</button>`;
    form.addEventListener('submit', this.postComment);
    return form;
  }

  this.loadComments = function() {
    const { host, pathname } = new URL(window.location.href);
    const urlParams = new URLSearchParams();
    urlParams.append('d', host);
    urlParams.append('p', pathname);

    // fetch('{$commentUrl}?' + urlParams.toString())
    //   .then(r => r.json())
    //   .then(posts => {
    //     self.allComments = self.generateTestPosts(Math.ceil(Math.random() * 10));
    //     self.renderPosts(self.allComments);
    //   })
    //   .catch(e => console.error(e));
    // test posts
    this.allComments = this.generateTestPosts(Math.ceil(Math.random() * 10));
    this.renderPosts(this.allComments);
  }

  this.generateTestPosts = function (numPosts) {
    if (typeof self.testPostIdIncrement === 'undefined') {
      self.testPostIdIncrement = 1;
    }
    const ascii = [32];
    for (let i = 65; i <= 90; i++) ascii.push(i);
    for (let i = 97; i <= 122; i++) ascii.push(i);
    const posts = [];
    for (let i = 0; i < numPosts; i++) {
      let name = '';
      const nameLength = Math.floor(Math.random() * 10) + 3;
      for (let i = 0; i < nameLength; i++) {
        name += String.fromCharCode(ascii[Math.floor(Math.random() * ascii.length)]);
      }
      let comment = '';
      const commentLength = Math.floor(Math.random() * 10) + 3;
      for (let i = 0; i < commentLength; i++) {
        comment += String.fromCharCode(ascii[Math.floor(Math.random() * ascii.length)]);
      }

      const post = {
        id: self.testPostIdIncrement++,
        name,
        comment,
        moment: (Date.now() - (Math.ceil(Math.random() * 72) * 60 * 60 * 1000)) / 1000,
        replies: this.generateTestPosts(Math.floor(Math.random() * 3)),
      };
      posts.push(post);
    }

    posts.sort((a, b) => {
      if (a.moment === b.moment) return 0;
      return a.moment < b.moment ? -1 : 1;
    })

    return posts;
  }

  // Given a microsecond moment timestamp from PHP, convert to local time
  this.momentToLocalTime = function (moment) {
    const momentMs = moment * 1000;
    const utcPostTime = new Date(momentMs);
    const utcOffsetMs = utcPostTime.getTimezoneOffset() * 60 * 1000;
    const localTime = new Date(momentMs - utcOffsetMs);
    return localTime.toLocaleString();
  }
  
  this.generatePost = function(post) {
    const postEl = document.createElement('div');
    postEl.id = `post${post.id}`;
    postEl.style.margin = '10px 0 0 20px';
    postEl.innerHTML = `<article class="post">
      <header><strong>${post.name}</strong> | ${this.momentToLocalTime(post.moment)}</header>
      <div>${post.comment}</div>
      <footer><button>Reply</button></footer>
    </article>
    <div class="replies"></div>`;
    postEl.querySelector('footer > button').addEventListener('click', function() {
      const footer = postEl.querySelector('article > footer');
      footer.querySelector('button').style.display = 'none';
      footer.appendChild(self.getCommentForm(post.id));
    });
    if ((post.replies ?? []).length > 0) {
      this.renderPosts(post.replies, postEl);
    }
    return postEl;
  }
  
  this.renderPosts = function(posts, parent = null) {
    const replies = (parent === null ? self.el : parent).querySelector('.replies');
    posts.forEach(post => {
      const postEl = this.generatePost(post);
      replies.appendChild(postEl);
    });
  }
  
  this.comment = function(overrideExtraData) {
    const data = this.getUrlData();
    extraData = typeof overrideExtraData !== 'undefined' ? overrideExtraData : this.getExtraData();
    if (extraData) {
      data.e = extraData;
    }
    if (this.session) {
      data.s = this.session;
    }

    return fetch('{$commentUrl}', {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "omit",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      referrerPolicy: "origin-when-cross-origin",
      body: JSON.stringify(data),
    }).then(response => response.json()).then(response => {
      self.session = response;
    });
  }

  this.initialize();
}
